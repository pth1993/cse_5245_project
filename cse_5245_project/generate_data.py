from sklearn.model_selection import train_test_split
import random


def read_raw_data(input_file):
    graph = []
    with open(input_file, 'r') as f:
        for line in f:
            line = set(line.strip().split(','))
            graph.append(line)
    return graph


def generate_data(graph, train_embedding_file, train_classification_file, test_classification_file):
    node_list = list(set([node for edge in graph for node in edge]))
    graph_train, graph_test = train_test_split(graph, test_size=0.5, random_state=75)
    cnt = 0
    graph_neg = []
    while cnt != len(graph):
        node1 = random.choice(node_list)
        node2 = random.choice(node_list)
        while node2 == node1:
            node2 = random.choice(node_list)
        if {node1, node2} not in graph and {node1, node2} not in graph_neg:
            graph_neg.append({node1, node2})
            cnt += 1
            print(cnt)
    graph_train_neg, graph_test_neg = train_test_split(graph_neg, test_size=0.5, random_state=75)
    with open(train_embedding_file, 'w') as f:
        for line in graph_train:
            f.write(','.join(line) + '\n')
    with open(train_classification_file, 'w') as f:
        for line in graph_train:
            f.write(','.join(line) + ',1\n')
        for line in graph_train_neg:
            f.write(','.join(line) + ',0\n')
    with open(test_classification_file, 'w') as f:
        for line in graph_test:
            f.write(','.join(line) + ',1\n')
        for line in graph_test_neg:
            f.write(','.join(line) + ',0\n')


if __name__ == '__main__':
    graph = read_raw_data('data/snap_ppi.csv')
    generate_data(graph, 'data/snap_ppi_embedding_train.csv', 'data/snap_ppi_classification_train.csv',
                  'data/snap_ppi_classification_test.csv')
